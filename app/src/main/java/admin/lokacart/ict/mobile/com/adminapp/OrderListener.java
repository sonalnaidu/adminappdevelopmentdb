package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by root on 22/1/16.
 */
public interface OrderListener {
    public void onAsyncTaskComplete(String result, int flag);
    public void onCardClickListener(int position);
    public void onCardLongClickListener(int position);
}