package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by Vishesh on 06-01-2016.
 */
public class AdminDetails {
    public static String name, email, abbr, mobileNumber, password, id, status;

    AdminDetails()
    {
        name = "";
        email = "";
        abbr = "";
        mobileNumber = "";
        password = "";
        id = "";
        status = "";
    }


    public static String getName() {
        return name;
    }
    public static String getEmail() {
        return email;
    }
    public static String getAbbr() {
        return abbr;
    }
    public static String getMobileNumber() {
        return mobileNumber;
    }
    public static String getPassword() { return password; }
    public static String getID() { return id; }


    public static void setName(String adminName) {
        name = adminName;
    }
    public static void setEmail(String adminEmail){
        email = adminEmail;
    }
    public static void setAbbr(String adminAbbr) {
        abbr = adminAbbr;
    }
    public static void setMobileNumber(String adminMobileNumber) { mobileNumber = adminMobileNumber; }
    public static void setPassword(String adminPassword) { password = adminPassword; }
    public static void setID(String adminID) { id = adminID; }
}
